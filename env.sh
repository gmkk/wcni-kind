#!/bin/bash



# # clab version
#   ____ ___  _   _ _____  _    ___ _   _ _____ ____  _       _     
#  / ___/ _ \| \ | |_   _|/ \  |_ _| \ | | ____|  _ \| | __ _| |__  
# | |  | | | |  \| | | | / _ \  | ||  \| |  _| | |_) | |/ _` | '_ \ 
# | |__| |_| | |\  | | |/ ___ \ | || |\  | |___|  _ <| | (_| | |_) |
#  \____\___/|_| \_| |_/_/   \_\___|_| \_|_____|_| \_\_|\__,_|_.__/ 

#     version: 0.54.2
#      commit: 9ecdf0e2
#        date: 2024-04-11T12:01:05Z
#      source: https://github.com/srl-labs/containerlab
#  rel. notes: https://containerlab.dev/rn/0.54/#0542



set -v
#1.Add alias
if [[ $(cat ~/.bashrc | grep cc | wc -l) -gt 0 ]];then echo "alias already exsits"
else
cat <<EOF>>~/.bashrc
alias wp="cd /root/wcni-kind"
alias gitc="git clone https://gitee.com/gmkk/wcni-kind.git"
alias k="kubectl"
alias dip="kubectl get node -o wide"
alias kk="kubectl -nkube-system"
alias ds="docker ps"
alias cc="kubectl config get-contexts" 
alias sc="kubectl config use-context $1" 
alias lo="docker exec -it $1 $2"
alias all="kubectl get pods -A"
alias cls="kind get clusters $1"
alias cld="kind delete clusters $1"
EOF
fi

source ~/.bashrc > /dev/null 2>&1


#while true;do if [[ $(kubectl wait --for=condition=Ready nodes --all | grep met | wc -l) -eq $(kubectl get nodes -o name | wc -l) ]];then break;else sleep 1;fi;done


apt install sshpass >/dev/null 2>&1
for ip in {192.168.2.71,192.168.2.72,192.168.2.73}
do
  sshpass -p hive ssh-copy-id -o StrictHostKeyChecking=no root@$ip -p 22 >/dev/null 2>&1
done

ssh 192.168.2.71 '
echo "
network:
  version: 2
  ethernets:
    ens160:
      addresses:
      - 192.168.2.71/24
      gateway4: 192.168.2.1
      nameservers:
        addresses:
        - 192.168.2.1
        search: []
" >/etc/netplan/01-network-manager-all.yaml && systemctl stop NetworkManager && systemctl disable NetworkManager && netplan apply'

ssh 192.168.2.72 '
echo "
network:
  version: 2
  ethernets:
    ens160:
      addresses:
      - 192.168.2.72/24
      gateway4: 192.168.2.1
      nameservers:
        addresses:
        - 192.168.2.1
        search: []
" >/etc/netplan/01-network-manager-all.yaml && systemctl stop NetworkManager && systemctl disable NetworkManager && netplan apply'


ssh 192.168.2.73 '
echo "
network:
  version: 2
  ethernets:
    ens160:
      addresses:
      - 192.168.2.73/24
      gateway4: 192.168.2.1
      nameservers:
        addresses:
        - 192.168.2.1
        search: []
" >/etc/netplan/01-network-manager-all.yaml && systemctl stop NetworkManager && systemctl disable NetworkManager && netplan apply'

