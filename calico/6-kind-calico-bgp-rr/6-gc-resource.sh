#!/bin/bash

set -v

ifconfig br-leaf0 down
brctl delbr br-leaf0

ifconfig br-leaf1 down
brctl delbr br-leaf1

kubectl delete -f cni.yaml
