#!/bin/bash
set -v
# exec &>./cluster2-install-log-rec.txt
date

# node info: 192.168.2.66
# create a cluster with the local registry enabled in containerd
cat <<EOF | kind create cluster --name=cluster2 --image=kindest/node:v1.23.4 --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  disableDefaultCNI: true
  podSubnet: "10.20.0.0/16"
  serviceSubnet: "10.21.0.0/16"

nodes:
  - role: control-plane
  - role: worker

containerdConfigPatches:
- |-
  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."harbor.evescn.com"]
    endpoint = ["https://harbor.evescn.com"]
EOF

# prep the environment
kubectl taint nodes $(kubectl get nodes -o name | grep control-plane) node-role.kubernetes.io/control-plane:NoSchedule-
kubectl get nodes -owide -A

# install CNI
# ./cilium version
# cilium-cli: v0.14.8 compiled with go1.20.4 on linux/amd64
# cilium image (default): v1.13.3
cilium install --context kind-cluster2 \
  --version v1.13.0-rc5 \
  --helm-set ipam.mode=kubernetes,cluster.name=cluster2,cluster.id=2 --inherit-ca kind-cluster1
cilium status  --context kind-cluster2 --wait

for i in $(docker ps -a --format "table {{.Names}}" | grep cluster2) 
do
    echo $i
    docker cp /usr/bin/ping $i:/usr/bin/ping
    docker exec -it $i bash -c "sed -i -e 's/jp.archive.ubuntu.com\|archive.ubuntu.com\|security.ubuntu.com/old-releases.ubuntu.com/g' /etc/apt/sources.list"
    docker exec -it $i bash -c "apt-get -y update >/dev/null && apt-get -y install net-tools tcpdump lrzsz bridge-utils >/dev/null 2>&1"
done