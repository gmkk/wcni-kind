#/bin/bash
set -v

kubectl delete -f ./cluster1.yaml --context kind-cluster1
kubectl delete -f ./cluster2.yaml --context kind-cluster2

kubectl apply -f ./cluster1.yaml --context kind-cluster1
kubectl apply -f ./cluster2.yaml --context kind-cluster2


