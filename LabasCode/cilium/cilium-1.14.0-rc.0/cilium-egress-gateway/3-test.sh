kubectl create ns egress-access
kubectl create ns egress-noaccess

kubectl -n egress-access create deployment cni --image=harbor.dayuan1997.com/devops/nettool:0.9
kubectl -n egress-noaccess create deployment cni --image=harbor.dayuan1997.com/devops/nettool:0.9

cat <<EOF | kubectl apply -f -
apiVersion: cilium.io/v2
kind: CiliumEgressGatewayPolicy
metadata:
  name: egress-access
spec:
  destinationCIDRs:
  - "11.1.8.0/24"
  selectors:
  - podSelector:
      matchLabels:
        io.kubernetes.pod.namespace: egress-access
  egressGateway:
    nodeSelector:
      matchLabels:
        egress-gw: 'true'
    interface: eth9
EOF
