
# BPF234:
docker stop t1 && docker rm t1
docker network create -d bridge --subnet=172.100.0.0/24 n1
docker run  --name t1 -d --net n1 harbor.dayuan1997.com/devops/nettool:0.9 tail -f /etc/fstab
docker exec -it t1 ip a l

ip route add 172.200.0.0/24 via 10.0.0.235 dev ens33


# BPF235:
docker stop t2 && docker rm t2
docker network create -d bridge --subnet=172.200.0.0/24 n2
docker run  --name t2 -d --net n2 harbor.dayuan1997.com/devops/nettool:0.9 tail -f /etc/fstab
docker exec -it t2 ip a l

ip route add 172.100.0.0/24 via 10.0.0.234 dev ens33


# 测试
# BPF234:
docker exec -it t1 ping 172.200.0.2

# BPF235:
docker exec -it t2 ping 172.100.0.2